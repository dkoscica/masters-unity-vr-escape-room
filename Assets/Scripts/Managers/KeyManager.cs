﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class KeyManager : Singleton<KeyManager>
{
    private List<Key> foundKeys = new List<Key>();

    #region MonoBehaviour API
    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        foundKeys = new List<Key>();
    }
    #endregion

    #region Public methods
    public void AddToFoundKeys(Key key) => foundKeys.Add(key);

    public Key FindKeyWithName(string name)
    {
        var foundKey = foundKeys.Find(key => key.name == name);
        if (foundKey == null)
        {
            Debug.LogWarning($"Key with: {name} not found!");
            return null;
        }
        return foundKey;
    }

    public bool IsKeyWithNameFound(string name)
    {
        var key = FindKeyWithName(name);
        return key?.isFound ?? false;
    }
    #endregion
}
