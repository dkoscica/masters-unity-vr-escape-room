﻿using UnityEngine;
using System;
using UnityEngine.Audio;

public class AudioManager : Singleton<AudioManager>
{
    public Sound[] sounds;

    #region MonoBehaviour API
    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        foreach (var sound in sounds)
        {
            sound.source = gameObject.AddComponent<AudioSource>();
            sound.source.clip = sound.clip;
            sound.source.volume = sound.volume;
            sound.source.pitch = sound.pitch;
            sound.source.loop = sound.loop;
        }
    }
    #endregion

    #region Public methods
    public void Play(string name)
    {
        var foundSound = Array.Find(sounds, sound => sound.name == name);
        if (foundSound == null)
        {
            Debug.LogWarning($"Sound: {name} not found!");
            return;
        }
        foundSound.source.Play();
    }
    #endregion
}
