using UnityEngine;

public static class CollisionExtensions
{
    public static bool HasGameObjectWithTag(this Collision collision, string tag) => collision.collider.gameObject.CompareTag(tag);
}