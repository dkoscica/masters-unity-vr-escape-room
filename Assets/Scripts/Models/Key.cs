using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class Key
{
    public string name;

    public GameObject keyObject;

    public bool isFound;

}
