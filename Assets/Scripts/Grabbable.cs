﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grabbable : MonoBehaviour
{

    #region Public Attributes 
    public bool IsHolding { get; private set; }
    #endregion

    #region Attributes
    private const int ThrowForce = 250;
    private HandController handController;
    private GameObject parentObject;
    private Rigidbody rigidBody;
    private Vector3 currentGrabbedLocation;
    #endregion

    #region MonoBehaviour API
    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        handController = new HandController();
        rigidBody = GetComponent<Rigidbody>();
        currentGrabbedLocation = new Vector3();
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if (IsHolding)
        {
            currentGrabbedLocation = transform.position;
            rigidBody.velocity = Vector3.zero;
            rigidBody.angularVelocity = Vector3.zero;
            transform.SetParent(parentObject.transform);

            if (handController.IsRightGripReleased())
            {
                ReleaseObject();
            }
            return;
        }
        // objectPosition = item.transform.position;
        // item.transform.SetParent(null);
        // item.GetComponent<Rigidbody>().useGravity = true;
        // item.transform.position = objectPosition;
    }

    /// <summary>
    /// OnCollisionEnter is called when this collider/rigidbody has begun
    /// touching another rigidbody/collider.
    /// </summary>
    /// <param name="other">The Collision data associated with this collision.</param>
    void OnCollisionEnter(Collision other)
    {
        if (IsCollisionTriggeredByHandController(other))
        {
            GrabObject(other.collider.gameObject);
        }
    }
    #endregion

    #region Public methods
    public bool IsCollisionTriggeredByHandController(Collision other)
    {
        bool isHand = other.HasGameObjectWithTag(GameTags.Hand);
        return isHand && (handController.IsRightGripPressed() || handController.IsLeftGripPressed());
    }
    public bool IsCollisionExitedByHandController(Collision other)
    {
        bool isHand = other.HasGameObjectWithTag(GameTags.Hand);
        return isHand && (handController.IsRightGripReleased() || handController.IsLeftGripReleased());
    }
    #endregion

    #region Private methods
    private void GrabObject(GameObject controllerObject)
    {
        IsHolding = true;
        parentObject = controllerObject;
        transform.parent = parentObject.transform.transform;
        rigidBody.useGravity = false;
        rigidBody.isKinematic = false;
        currentGrabbedLocation = transform.position;
    }

    private void ReleaseObject()
    {
        transform.parent = null; // Un-parent throwable object so it doesn't follow the controller
        rigidBody.useGravity = true; // Re-enables the physics engine.
        IsHolding = false;
    }

    public void ThrowObject()
    {
        transform.parent = null; // Un-parent throwable object so it doesn't follow the controller
        rigidBody.useGravity = true; // Re-enables the physics engine.

        Vector3 throwVector = transform.position - currentGrabbedLocation;
        rigidBody.AddForce(throwVector * ThrowForce, ForceMode.Impulse); // Throws the object by applying the given force
        IsHolding = false;
    }
    #endregion
}

