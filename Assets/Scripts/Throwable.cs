using UnityEngine;

public class Throwable : MonoBehaviour
{
    private Material outlineMaterial;
    private Rigidbody rigidbody;
    private Vector3 currentGrabbedLocation; // The tracked location of our object for us to throw
    private bool isGrabbed;

    private const string OutlineWidthKey = "_Outline";
    private const float OutlineWidthValue = 0.03f;

    void Start ()
    {
        //outlineMaterial = GetComponent<Renderer>().materials[1];
        //outlineMaterial.SetFloat(OutlineWidthKey, 0);

        rigidbody = GetComponent<Rigidbody>();
        currentGrabbedLocation = new Vector3();
        isGrabbed = false;
    }

    void Update()
    {
        if (isGrabbed)
        {
            currentGrabbedLocation = transform.position;
        }
    }

    // Shows the outline by setting the width to be a fixed value when we are 
    // pointing at it.
    public void ShowOutlineMaterial()
    {
        //outlineMaterial.SetFloat(OutlineWidthKey, OutlineWidthValue);
    }

    // Hides the outline by making the width 0 when we are no longer 
    // pointing at it.
    public void HideOutlineMaterial()
    {
        //outlineMaterial.SetFloat(OutlineWidthKey, 0);
    }

    // Setup our throwable game object for when it is grabbed. Set the object that
    // grabbed it as its parent and disables kinematics
    public void GetGrabbed(GameObject controllerObject)
    {
        // transform.parent = controllerObject.transform; // Set object as a child so it'll follow our controller
        // //rigidbody.isKinematic = false; // Stops physics from affecting the grabbed object
        // rigidbody.useGravity = false;
        // isGrabbed = true;
    }

    // Releases the throwable game object from being grabbed. It is sent flying 
    // by the force given by the controller game object.
    public void GetReleased()
    {
        if (isGrabbed)
        {
            transform.parent = null; // Un-parent throwable object so it doesn't follow the controller
            rigidbody.isKinematic = false; // Re-enables the physics engine.

            Vector3 throwVector = transform.position - currentGrabbedLocation;
            rigidbody.AddForce(throwVector * 10, ForceMode.Impulse); // Throws the ball by applying the given force
            isGrabbed = false;
        }
    }
}