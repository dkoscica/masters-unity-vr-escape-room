using UnityEngine;

public class HandController
{

    private static class HandControllerInputAxis
    {
        public const string RightGrip = "RightGrip";
        public const string LeftGrip = "LeftGrip";
    }

    private const float MinimumVirtualAxisValue = 0.5f;
    private GameObject leftHandObject;
    private GameObject rightHandObject;

    public bool IsRightGripPressed() => Input.GetAxis(HandControllerInputAxis.RightGrip) > MinimumVirtualAxisValue;

    public bool IsRightGripReleased() => Input.GetAxis(HandControllerInputAxis.RightGrip) < MinimumVirtualAxisValue;

    public bool IsLeftGripPressed() => Input.GetAxis(HandControllerInputAxis.LeftGrip) > MinimumVirtualAxisValue;

    public bool IsLeftGripReleased() => Input.GetAxis(HandControllerInputAxis.LeftGrip) < MinimumVirtualAxisValue;

    public void SetLeftHand(GameObject gameObject) => this.leftHandObject = gameObject;

    public void SetRightHand(GameObject gameObject) => this.rightHandObject = gameObject;
}

