using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BedRoomController : MonoBehaviour
{

    #region Private Attributes
    private const string KeyName = "BedRoomKey";
    private TV tv;
    private BedRoomLightSwitch bedRoomLightSwitch;
    #endregion

    #region MonoBehaviour API
    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        TVRemote.OnTVRemoteGrabbed += OnTVRemoteGrabbed;
        TVRemote.OnTVRemoteReleased += OnTVRemoteReleased;

        tv = FindObjectOfType<TV>();
        bedRoomLightSwitch = FindObjectOfType<BedRoomLightSwitch>();
        bedRoomLightSwitch.TurnOnLight();
    }
    #endregion

    #region Private methods
    private void OnTVRemoteGrabbed()
    {
        tv.TurnOnTV();
        bedRoomLightSwitch.TurnOffLight();
    }
    private void OnTVRemoteReleased()
    {
        tv.TurnOffTV();
        bedRoomLightSwitch.TurnOnLight();
    }
    #endregion

    #region Public methods
    public void IsKeyFound() => KeyManager.Instance.IsKeyWithNameFound(KeyName);
    #endregion

}