﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System;

public class BedRoomKey : BaseKey
{

    #region Public Attributes
    [SerializeField] private Key key;
    #endregion

    #region Private Attributes
    private Grabbable grabbable;
    #endregion

    #region MonoBehaviour API
    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        Assert.IsNotNull(key);
    }

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        grabbable = GetComponent<Grabbable>();
    }

    /// <summary>
    /// OnCollisionEnter is called when this collider/rigidbody has begun
    /// touching another rigidbody/collider.
    /// </summary>
    /// <param name="other">The Collision data associated with this collision.</param>
    void OnCollisionEnter(Collision other)
    {
        if (grabbable.IsCollisionTriggeredByHandController(other))
        {
            KeyGrabbed();
        }
    }
    #endregion

    #region Private methods
    private void KeyGrabbed()
    {
        KeyManager.Instance.AddToFoundKeys(key);
        UnlockDoor();
    }
    #endregion
}
