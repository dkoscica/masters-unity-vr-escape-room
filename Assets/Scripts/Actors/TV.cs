﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System;

public class TV : MonoBehaviour
{

    #region Public Attributes
    [SerializeField] private Material tvOnMaterial;
    [SerializeField] private Material tvOffMaterial;
    #endregion

    #region Private Attributes
    private AudioSource audioSource;
    private Renderer renderer;
    private bool isTVTurnedOn = false;
    private IEnumerator tvTimerRoutine;
    private const int timeoutInSeconds = 5;
    #endregion

    #region MonoBehaviour API
    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        Assert.IsNotNull(tvOnMaterial);
        Assert.IsNotNull(tvOffMaterial);
    }

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        renderer = GetComponent<Renderer>();
        audioSource = GetComponent<AudioSource>();
        audioSource.loop = true;
    }

    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
    void FixedUpdate()
    {
        renderer.material = isTVTurnedOn ? tvOnMaterial : tvOffMaterial;
    }
    #endregion

    #region Private methods
    private void StartTVTimer()
    {
        StopTVTimer();
        tvTimerRoutine = TVTimerRoutine();
        StartCoroutine(tvTimerRoutine);
    }

    private void StopTVTimer()
    {
        if (tvTimerRoutine != null)
        {
            StopCoroutine(tvTimerRoutine);
        }
    }

    private IEnumerator TVTimerRoutine()
    {
        yield return new WaitForSeconds(timeoutInSeconds);
        TurnOffTV();
    }
    #endregion

    #region Public methods
    public void TurnOnTV()
    {
        audioSource.Play();
        isTVTurnedOn = true;
        StartTVTimer();
    }

    public void TurnOffTV()
    {
        audioSource.Stop();
        isTVTurnedOn = false;
    }
    #endregion

}
