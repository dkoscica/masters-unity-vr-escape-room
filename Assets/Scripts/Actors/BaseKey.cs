using UnityEngine;
using System.Collections;

public abstract class BaseKey : MonoBehaviour
{

    #region Public Attributes
    [SerializeField] protected string playerTag = "Player";
    [SerializeField] protected GameObject door;
    #endregion

    protected void UnlockDoor()
    {
        DoorScript doorScript = door.GetComponent<DoorScript>();
        doorScript.keySystem.isUnlock = true;
        //Destroy(gameObject);
    }
}