﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System;

public class MobilePhone : MonoBehaviour
{

    #region Public Attributes
    [SerializeField] private Material mobilePhoneOnMaterial;
    [SerializeField] private Material mobilePhoneOffMaterial;
    [SerializeField] private AudioClip ringingSound;
    [SerializeField] private AudioClip hangupSound;
    #endregion

    #region Private Attributes
    private Grabbable grabbable;
    private AudioSource audioSource;
    private Renderer renderer;
    private Material phoneMaterial;
    private bool hasMessageBeenShown = false;
    #endregion

    #region MonoBehaviour API
    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        Assert.IsNotNull(mobilePhoneOnMaterial);
        Assert.IsNotNull(mobilePhoneOffMaterial);
        Assert.IsNotNull(ringingSound);
        Assert.IsNotNull(hangupSound);
    }

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        grabbable = GetComponent<Grabbable>();
        renderer = GetComponentInChildren<Renderer>();
        audioSource = GetComponent<AudioSource>();
        InitMobilePhone();
    }

    /// <summary>
    /// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
    /// </summary>
    void FixedUpdate()
    {
        renderer.material = phoneMaterial;
    }

    /// <summary>
    /// OnCollisionEnter is called when this collider/rigidbody has begun
    /// touching another rigidbody/collider.
    /// </summary>
    /// <param name="other">The Collision data associated with this collision.</param>
    void OnCollisionEnter(Collision other)
    {
        if (grabbable.IsCollisionTriggeredByHandController(other))
        {
            MobilePhoneGrabbed();
        }
    }

    /// <summary>
    /// OnCollisionExit is called when this collider/rigidbody has
    /// stopped touching another rigidbody/collider.
    /// </summary>
    /// <param name="other">The Collision data associated with this collision.</param>
    void OnCollisionExit(Collision other)
    {
        if (grabbable.IsCollisionExitedByHandController(other))
        {
            MobilePhoneReleased();
        }
    }

    /// <summary>
    /// OnTriggerEnter is called when the Collider other enters the trigger.
    /// </summary>
    /// <param name="other">The other Collider involved in this collision.</param>
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == GameTags.Player && !hasMessageBeenShown)
        {
            TurnOnMobilePhone();
        }
    }
    #endregion

    #region Private methods
    private void InitMobilePhone()
    {
        TogglePhoneMaterial(true);
        SetupAudioSource(null, false);
    }

    private void TurnOnMobilePhone()
    {
        TogglePhoneMaterial(true);
        SetupAudioSource(ringingSound);
        audioSource.Play();
    }

    private void MobilePhoneGrabbed()
    {
        TogglePhoneMaterial(true);
        if (hasMessageBeenShown)
        {
            return;
        }
        SetupAudioSource(hangupSound);
        audioSource.Play();
    }

    private void MobilePhoneReleased()
    {
        audioSource.Stop();
        hasMessageBeenShown = true;
    }

    private void TogglePhoneMaterial(bool isPhoneOn) => phoneMaterial = isPhoneOn ? mobilePhoneOnMaterial : mobilePhoneOffMaterial;

    private void SetupAudioSource(AudioClip audioClip, bool loop = true)
    {
        audioSource.clip = audioClip;
        audioSource.loop = loop;
    }
    #endregion
}
