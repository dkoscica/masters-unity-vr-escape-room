﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BedRoomLightSwitch : MonoBehaviour
{

    #region Private Attributes
    private LightSwitch lightSwitch;
    #endregion

    #region MonoBehaviour API
    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        lightSwitch = GetComponent<LightSwitch>();
    }
    #endregion

    #region Public methods
    public void TurnOnLight() => lightSwitch.Light_On();
    public void TurnOffLight() => lightSwitch.Light_Off();
    #endregion

}
