﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Grabbable))]
public class TVRemote : MonoBehaviour
{

    #region Actions
    public static event Action OnTVRemoteGrabbed = delegate { };
    public static event Action OnTVRemoteReleased = delegate { };
    #endregion

    #region Private Attributes
    private Grabbable grabbable;
    #endregion

    #region MonoBehaviour API
    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        grabbable = GetComponent<Grabbable>();
    }

    /// <summary>
    /// OnCollisionEnter is called when this collider/rigidbody has begun
    /// touching another rigidbody/collider.
    /// </summary>
    /// <param name="other">The Collision data associated with this collision.</param>
    void OnCollisionEnter(Collision other)
    {
        if (grabbable.IsCollisionTriggeredByHandController(other))
        {
            OnTVRemoteGrabbed();
        }
    }

    /// <summary>
    /// OnCollisionExit is called when this collider/rigidbody has
    /// stopped touching another rigidbody/collider.
    /// </summary>
    /// <param name="other">The Collision data associated with this collision.</param>
    void OnCollisionExit(Collision other)
    {
        if (grabbable.IsCollisionExitedByHandController(other))
        {
            OnTVRemoteReleased();
        }
    }
    #endregion
}
