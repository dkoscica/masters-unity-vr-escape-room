internal static class GameTags
{
    public const string Hand = "Hand";
    public const string Player = "Player";
    public const string BedroomLightSwitch = "BedroomLightSwitch";
}