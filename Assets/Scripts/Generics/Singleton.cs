﻿using System.Collections;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{

    private static T instance;

    public static T Instance
    {
        get
        {
            T objectOfType = FindObjectOfType<T>();
            if (instance == null)
            {
                instance = objectOfType;
            }
            else if (instance != objectOfType)
            {
                Destroy(objectOfType);
            }
            DontDestroyOnLoad(objectOfType);
            return instance;
        }
    }

}